package com.gaoyuan.namewords.bean;

/**
 * @description:
 * @Author gaoyuan
 * @Date Created in 17:15 2022/6/16
 */
public class NameMessageBean {
    private String name;
    private String message;
    private String createTime;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
