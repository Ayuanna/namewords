package com.gaoyuan.namewords.serviceImpl;

import com.gaoyuan.namewords.bean.NameMessageBean;
import com.gaoyuan.namewords.mapper.NameMessageMapper;
import com.gaoyuan.namewords.service.NameMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @description:
 * @Author gaoyuan
 * @Date Created in 17:16 2022/6/16
 */
@Service("nameMessageService")
public class NameMessageServiceImpl implements NameMessageService {
    @Autowired
    private NameMessageMapper nameMessageMapper;

    @Override
    public String setMessage(NameMessageBean param) {
        String responseMessage = "留言成功";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowDateTime = df.format(System.currentTimeMillis());
        param.setCreateTime(nowDateTime);
        nameMessageMapper.setMessage(param);
        return responseMessage;
    }

    @Override
    public List<NameMessageBean> getMessage(String name) {
        List<NameMessageBean> message = nameMessageMapper.getMessageByName(name);
        return message;
    }

    @Override
    public String getCopywriting(String writingId) {
        return nameMessageMapper.getCopywriting(writingId);
    }
}
