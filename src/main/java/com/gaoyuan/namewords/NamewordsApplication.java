package com.gaoyuan.namewords;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.gaoyuan.namewords.mapper")
public class NamewordsApplication {

    public static void main(String[] args) {
        SpringApplication.run(NamewordsApplication.class, args);
    }

}
