package com.gaoyuan.namewords.controller;

import com.gaoyuan.namewords.bean.NameMessageBean;
import com.gaoyuan.namewords.service.NameMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @description:
 * @Author gaoyuan
 * @Date Created in 16:29 2022/6/16
 */
@Controller
@RequestMapping("/message")
public class NameWordsController {
    @Autowired
    NameMessageService nameMessageService;
    @RequestMapping("/setMessage")
    @ResponseBody
    public String setMessage(NameMessageBean param){
       return nameMessageService.setMessage(param);
    }

    @RequestMapping("/getMessage")
    @ResponseBody
    public List<NameMessageBean> getMessage(String name){
        return nameMessageService.getMessage(name);

    }

    @RequestMapping("/getCopywriting")
    @ResponseBody
    public String getCopywriting(String writingId){
        return nameMessageService.getCopywriting(writingId);

    }

}
