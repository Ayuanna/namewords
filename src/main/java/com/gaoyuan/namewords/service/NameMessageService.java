package com.gaoyuan.namewords.service;

import com.gaoyuan.namewords.bean.NameMessageBean;

import java.util.List;

/**
 * @description:
 * @Author gaoyuan
 * @Date Created in 17:16 2022/6/16
 */
public interface NameMessageService {
    String setMessage(NameMessageBean param);

    List<NameMessageBean> getMessage(String name);

    String getCopywriting(String writingId);
}
