package com.gaoyuan.namewords.mapper;

import com.gaoyuan.namewords.bean.NameMessageBean;

import java.util.List;

/**
 * @description:
 * @Author gaoyuan
 * @Date Created in 17:16 2022/6/16
 */
public interface NameMessageMapper {
    List<NameMessageBean> getMessageByName(String name);
    void setMessage(NameMessageBean param);
    String getCopywriting(String writingId);
}
